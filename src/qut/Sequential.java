package qut;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.au.jacobi.pattern.Match;
import edu.au.jacobi.pattern.Series;
import jaligner.BLOSUM62;
import jaligner.Sequence;
import jaligner.SmithWatermanGotoh;
import jaligner.matrix.Matrix;

public class Sequential
{
    private static HashMap<String, Sigma70Consensus> consensus = new HashMap<String, Sigma70Consensus>();
    private static ThreadLocal<Series> sigma70_pattern = new ThreadLocal<Series>() {
    	@Override protected Series initialValue() {
    		return Sigma70Definition.getSeriesAll_Unanchored(0.7);
    	}
    };
    private static final Matrix BLOSUM_62 = BLOSUM62.Load();
    private static byte[] complement = new byte['z'];

    static
    {
        complement['C'] = 'G'; complement['c'] = 'g';
        complement['G'] = 'C'; complement['g'] = 'c';
        complement['T'] = 'A'; complement['t'] = 'a';
        complement['A'] = 'T'; complement['a'] = 't';
    }

                    
    private static List<Gene> ParseReferenceGenes(String referenceFile) throws FileNotFoundException, IOException
    {
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(referenceFile)));
        List<Gene> referenceGenes = new ArrayList<Gene>();
        while (true)
        {
            String name = reader.readLine();
            if (name == null)
                break;
            String sequence = reader.readLine();
            referenceGenes.add(new Gene(name, 0, 0, sequence));
            consensus.put(name, new Sigma70Consensus());
        }
        reader.close();
        return referenceGenes;
    }

    private static boolean Homologous(PeptideSequence A, PeptideSequence B)
    {
        return SmithWatermanGotoh.align(new Sequence(A.toString()), new Sequence(B.toString()), BLOSUM_62, 10f, 0.5f).calculateScore() >= 60;
    }

    private static NucleotideSequence GetUpstreamRegion(NucleotideSequence dna, Gene gene)
    {
        int upStreamDistance = 250;
        if (gene.location < upStreamDistance)
           upStreamDistance = gene.location-1;

        if (gene.strand == 1)
            return new NucleotideSequence(java.util.Arrays.copyOfRange(dna.bytes, gene.location-upStreamDistance-1, gene.location-1));
        else
        {
            byte[] result = new byte[upStreamDistance];
            int reverseStart = dna.bytes.length - gene.location + upStreamDistance;
            for (int i=0; i<upStreamDistance; i++)
                result[i] = complement[dna.bytes[reverseStart-i]];
            return new NucleotideSequence(result);
        }
    }

    private static Match PredictPromoter(NucleotideSequence upStreamRegion)
    {
        return BioPatterns.getBestMatch(sigma70_pattern.get(), upStreamRegion.toString());
    }

    private static void ProcessDir(List<String> list, File dir)
    {
        if (dir.exists())
            for (File file : dir.listFiles())
                if (file.isDirectory())
                    ProcessDir(list, file);
                else
                    list.add(file.getPath());
    }

    private static List<String> ListGenbankFiles(String dir)
    {
        List<String> list = new ArrayList<String>();
        ProcessDir(list, new File(dir));
        return list;
    }

    private static GenbankRecord Parse(String file) throws IOException
    {
        GenbankRecord record = new GenbankRecord();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
        record.Parse(reader);
        reader.close();
        return record;
    }

    public static void run(String referenceFile, String dir) throws FileNotFoundException, IOException, InterruptedException
    {      	
    	long t0 = System.nanoTime();
        List<Gene> referenceGenes = ParseReferenceGenes(referenceFile);
        ExecutorService executorService = Executors.newFixedThreadPool(8);
        List<String> listGenbankFiles = ListGenbankFiles(dir);
        CountDownLatch signal1 = new CountDownLatch(listGenbankFiles.size());
        
        for (String filename : listGenbankFiles)
        {		
        	executorService.execute(() -> {
	            GenbankRecord recordTemp = null;
				try {
					recordTemp = Parse(filename);
				} catch (IOException e) {}
				final GenbankRecord record = recordTemp;		
	            for (Gene gene : record.genes)
	            {  	  
	            	executorService.execute(() -> {
	            		for (Gene referenceGene : referenceGenes) {  
		                    if (Homologous(gene.sequence, referenceGene.sequence))
		                    {
		                        NucleotideSequence upStreamRegion = GetUpstreamRegion(record.nucleotides, gene);
		                        Match prediction = PredictPromoter(upStreamRegion);
		                        if (prediction != null)
		                        {
		                        	synchronized(consensus.get(referenceGene.name)) {
		                        		consensus.get(referenceGene.name).addMatch(prediction);
		                        	}
		                        }
		                    }
	            		}
	    			}); 		    		
	        	}
	            signal1.countDown();
	        });
        }
        signal1.await();
        executorService.shutdown();
        executorService.awaitTermination(100, TimeUnit.MINUTES);
        
    	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss");
    	LocalDateTime dateTime = LocalDateTime.now();
		PrintWriter out = new PrintWriter("C:\\Users\\Jacob Botha\\Desktop\\CAB401 Assignment Output\\ExecutorService\\take23\\"+dtf.format(dateTime)+".txt");
        
		Sigma70Consensus all = new Sigma70Consensus();
		for (Map.Entry<String, Sigma70Consensus> entry : consensus.entrySet()) {
			all.addConsensus(entry.getValue());
			out.println(entry.getKey() + " " + entry.getValue());
		}
		out.println("all " + all);
        out.println((System.nanoTime() - t0)/1000000000.0);
        out.close();
    }

    public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException
    {  	  	

		run("referenceGenes.list", "Ecoli");

    }
}
